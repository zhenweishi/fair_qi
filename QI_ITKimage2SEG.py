"""
###############################
@author: zhenwei.shi, Maastro##
###############################

This scrip shows that how to generate ITK object (nrrd,etc) of specific ROI from DICOM-RT, then generate DICOM Segmentation objects
Before running this script, you need to add dcmqi tool to your system path by "export PATH="$(pwd)/dcmqi/bin":$PATH"

"""
from __future__ import print_function

import os
from matplotlib.pyplot import cm
import matplotlib.pyplot as plt
import glob
import shutil
from DicomDatabase import DicomDatabase
import PyrexReader
import numpy as np
import re
import numpy as np
import SimpleITK as sitk
from subprocess import call


# ----------------------Functions ---------------------------
# # convert nrrd to SEG.dcm using dcmqi
def DCMRT2SEG(inputlabelList,inputDICOMDirectory,inputMetadata,outputSEGfile,outputDir):
    outputDICOM = os.path.join(outputDir,outputSEGfile)
    try:
        call(['itkimage2segimage', '--inputImageList', inputlabelList,'--inputDICOMDirectory',inputDICOMDirectory,\
              '--inputMetadata',inputMetadata, '--outputDICOM', outputDICOM])
    except:
        print('Error: failed to pack dcm image to SEG.dcm')

# convert SEG objects to ITK nrrd images
def SEG2ITKimage(inputSEGfile, segmentsDir,ptid):
    if not os.path.exists(segmentsDir):
        os.mkdir(segmentsDir)    
    try:
        call(['segimage2itkimage', '--inputDICOM', inputSEGfile, '--outputDirectory', segmentsDir,'-p', ptid])
    except:
        print('Error: Failed to pack SEG to nrrd image')


if __name__ == "__main__":

    #-------------------------USER-------------------------------
    roi = 'GTV-1'                                       # specify ROI 
    Jsonfile = 'metadata/metadata_LUNG1.json'           # specify metadata Json file
    output_dir = './output/DCMSEG_example/'             # specify output directory
    mask_dir = 'example/itkimage_example/'              # specify tmp itkimage directory
    ptid = 'LUNG1-002'                                  # specify patient ID
    inputImageDir = 'Data/Lung1/LUNG1-002/01-01-2014-StudyID-85095/1-61228' # specify CT images dirctory
    inputSEG = ptid + '_' + roi + '_SEG.dcm'
    
    #-----------------------------------------------------------
    mask_list = glob.glob(os.path.join(mask_dir,'*.nrrd'))     
    inputlabelList = ','.join(map(str,mask_list))    

    DCMRT2SEG(inputlabelList,inputImageDir,Jsonfile,inputSEG,output_dir)