"""
###############################
@author: zhenwei.shi, Maastro##
###############################

This scrip shows that how to generate ITK object (nrrd,etc) of specific ROI from DICOM-RT, then generate DICOM Segmentation objects
Before running this script, you need to add dcmqi tool to your system path by "export PATH="$(pwd)/dcmqi/bin":$PATH"

"""
from __future__ import print_function

import os
from matplotlib.pyplot import cm
import matplotlib.pyplot as plt
import glob
import shutil
from DicomDatabase import DicomDatabase
import PyrexReader
import numpy as np
import re
import numpy as np
import SimpleITK as sitk
from subprocess import call


#-------------------------USER-------------------------------
roi = 'GTV-1'                                       # specify ROI 
walk_dir = 'Data/LUNG1'                             # specify data directory
Jsonfile = 'metadata/metadata_LUNG1.json'           # specify metadata Json file
output_dir = './output/DCMSEG_example/'             # specify output directory
mask_dir = 'output/tmp_itkimage/'                   # specify tmp itkimage directory

# ----------------------Functions ---------------------------
# # convert nrrd to SEG.dcm using dcmqi
def DCMRT2SEG(inputlabelList,inputDICOMDirectory,inputMetadata,outputSEGfile,outputDir):
    outputDICOM = os.path.join(outputDir,outputSEGfile)
    try:
        call(['itkimage2segimage', '--inputImageList', inputlabelList,'--inputDICOMDirectory',inputDICOMDirectory,\
              '--inputMetadata',inputMetadata, '--outputDICOM', outputDICOM])
    except:
        print('Error: failed to pack dcm image to SEG.dcm')

# convert SEG objects to ITK nrrd images
def SEG2ITKimage(inputSEGfile, segmentsDir,ptid):
    if not os.path.exists(segmentsDir):
        os.mkdir(segmentsDir)    
    try:
        call(['segimage2itkimage', '--inputDICOM', inputSEGfile, '--outputDirectory', segmentsDir,'-p', ptid])
    except:
        print('Error: Failed to pack SEG to nrrd image')

#-----------------create temporal CT/STRUCT directories-----------
CTWorkingDir = "./CTFolder"
STRUCTWorkingDir = "./StructFolder"
if not os.path.exists(CTWorkingDir):
  os.makedirs(CTWorkingDir)
if not os.path.exists(STRUCTWorkingDir):
  os.makedirs(STRUCTWorkingDir)
# -----------------------------------------------------------
# initialize dicom DB
dicomDb = DicomDatabase()
# walk over all files in folder, and index in the database
dicomDb.parseFolder(walk_dir)
# -----------------------------------------------------------
excludeStructRegex = "(Patient.*|BODY.*|Body.*|NS.*|Couch.*)"
if os.environ.get("EXCLUDE_STRUCTURE_REGEX") is not None:
    excludeStructRegex = os.environ.get("EXCLUDE_STRUCTURE_REGEX")

# -----------------------------------------------------------
# loop over patients
for ptid in dicomDb.getPatientIds():
    print("staring with Patient %s" % (ptid))
    # get patient by ID
    myPatient = dicomDb.getPatient(ptid)
    # loop over RTStructs of this patient
    for myStructUID in myPatient.getRTStructs():
        print("Starting with RTStruct %s" % myStructUID)
        # Get RTSTRUCT by SOP Instance UID
        myStruct = myPatient.getRTStruct(myStructUID)
        # Get CT which is referenced by this RTStruct, and is linked to the same patient
        # mind that this can be None, as only a struct, without corresponding CT scan is found
        myCT = myPatient.getCTForRTStruct(myStruct)
        # check if the temperal CT/STRUCT folder is empty
        if not (os.listdir(CTWorkingDir)==[] and os.listdir(STRUCTWorkingDir)==[]):
            ct_files = glob.glob(os.path.join(CTWorkingDir,'*'))
            for f in ct_files:
                os.remove(f)
            struct_files = glob.glob(os.path.join(STRUCTWorkingDir,'*'))
            for f in struct_files:
                os.remove(f)        
        # only show if we have both RTStruct and CT
        if myCT is not None:
            # copy RTSTRUCT file to tmp folder as 'struct.dcm'
            shutil.copy2(myStruct.getFileLocation(),os.path.join(STRUCTWorkingDir,'struct.dcm'))
            # copy DICOM slices to tmp folder as 'struct.dcm'
            slices = myCT.getSlices()
            for i in range(len(slices)):
                shutil.copy2(slices[i],os.path.join(CTWorkingDir,str(i)+".dcm"))   
            # generate binary mask (same size as image) from RTSTUC
            # Read ROIs in RTSTRUCTURE
            mask_vol=PyrexReader.Read_RTSTRUCT(STRUCTWorkingDir)
            M=mask_vol[0]
            target = []
            for j in range(0,len(M.StructureSetROISequence)):
                target.append(M.StructureSetROISequence[j].ROIName)
                
            for k in range(len(target)):
                if not re.search(roi,target[k]):
                    print('skip ROI: %s' % target[k])
                    continue
                Image,Mask = PyrexReader.Img_Bimask(CTWorkingDir,STRUCTWorkingDir,target[k])
                sitk.WriteImage(Mask,mask_dir + ptid + '_' + target[k]+'.nrrd',True)
                # sitk.WriteImage(Image,mask_dir + ptid + '_' + target[k]+'_volume.nrrd',True)
            print('-'*30)

    inputSEG = ptid + '_' + roi + '_SEG.dcm'
    inputImageDir = CTWorkingDir

    mask_list = glob.glob(os.path.join(mask_dir,'*.nrrd'))     
    inputlabelList = ','.join(map(str,mask_list))                             
    # -----using dcmqi binary package--------------------
    DCMRT2SEG(inputlabelList,inputImageDir,Jsonfile,inputSEG,output_dir)
    # remove generated itkimage every patient
    mask_files = glob.glob(os.path.join(mask_dir,'*.nrrd'))
    for f in mask_files:
        os.remove(f)